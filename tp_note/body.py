import random
import time

from pygame import Vector2

import core
from fustrum import Fustrum

RED = (255, 0, 0)
VERT = (50, 205, 50)
VIOLET = (238, 130, 238)
GRIS = (128, 128, 128)
WHITE = (255, 255, 255)
JAUNE = (255, 255, 0)

class Body(object):
    def __init__(self,position,vMax, accMax, jaugeFaimMax, tiredMax, reproductionMax):

        self.position = position
        self.vitesse = Vector2()
        self.vMax = vMax
        self.accMax = accMax
        self.mass = 10

        self.jaugeFaimMax = jaugeFaimMax
        self.jaugeFaim = 0

        self.tiredMax = tiredMax
        self.tired = 0

        self.reproductionMax = reproductionMax
        self.reproduction = 0

        self.dateAnniversaire = time.time()

        self.esperancevie = random.randrange(300, 3000)

        self.Mort = False
        self.isSleeping = False

        self.sleepingTimeMax = random.randrange(1000, 2500)
        self.sleepingTime = 0
        self.color = (0, 0, 0)
        self.fustrum = Fustrum(150, self)
        self.acc = Vector2()
        # self.parent = parent

    def update(self):
        # voir avec les tableaux qui sont dans agents
        # dans son filtre perception

        self.tired += 0.01
        self.jaugeFaim += 0.01
        self.reproduction += 0.01

        if time.time() - self.dateAnniversaire >= self.esperancevie:
            self.Mort = True
        if self.tired >= self.tiredMax:
            self.isSleeping = True
            self.tired -= 1

        if self.tired == 0:
            self.isSleeping = False

        if self.isSleeping or self.Mort:
            self.vitesse = Vector2()
            self.acc = Vector2()

        if self.jaugeFaim >= self.jaugeFaimMax:
            self.Mort = True

        if self.acc.length() > self.accMax / self.mass:
            self.acc.scale_to_length(self.accMax / self.mass)

        self.vitesse = self.vitesse + self.acc

        if self.vitesse.length() > self.vMax:
            self.vitesse.scale_to_length(self.vMax)
        self.position = self.position + self.vitesse
        self.acc = Vector2()
        self.edge()

        if not self.Mort:
            self.jaugeFaim += 0.01
            self.tired += 0.01
            self.reproduction += 0.01



    def show(self, status):
        if self.Mort:
            self.color = GRIS
        else:
            if status == "Herbivore":
                self.color = RED
            elif status == "Decomposeur":
                self.color = JAUNE
            elif status == "Carnivore":
                self.color = VIOLET
            elif status == "SuperPredateur":
                self.color = VERT
        core.Draw.circle(self.color, self.position, self.mass)

    # Detection des colisions
    def edge(self):
        if self.position.x <= self.mass:
            self.vitesse.x *= -1
        if self.position.x + self.mass >= core.WINDOW_SIZE[0]:
            self.vitesse.x *= -1
        if self.position.y <= self.mass:
            self.vitesse.y *= -1
        if self.position.y + self.mass >= core.WINDOW_SIZE[1]:
            self.vitesse.y *= -1





