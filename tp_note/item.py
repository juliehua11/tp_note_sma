import random

from pygame import Vector2

import core


class Item(object):
    def __init__(self,position):
        self.position = position
        self.mass = 5
        self.color = (0, 255, 0)

    def show(self):
        core.Draw.circle(self.color, self.position, self.mass)
