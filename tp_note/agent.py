import random

from pygame import Vector2

from item import Item

import core
from body import Body


class Agent(object):
    def __init__(self, body, status):
        self.body = body
        self.uuid = random.randint(100000, 999999999)
        self.status = status
        self.proies = []
        self.predateurs = []
        self.symbiose = []
    def filtre_perception(self):
        self.proies = []
        self.symbiose = []
        self.predateurs = []
        for i in self.body.fustrum.perceptionList:
            if isinstance(i,Agent) :
                i.dist = self.body.position.distance_to(i.body.position)
                if not i.body.Mort:
                    if self.status == "SuperPredateur":
                        if i.status == "Carnivore":
                            self.proies.append(i)
                    elif self.status == "Carnivore":
                        if i.status == "SuperPredateur":
                            self.predateurs.append(i)
                        if i.status == "Herbivore":
                            self.proies.append(i)
                    elif self.status == "Herbivore":
                        if i.status == "Carnivore":
                            self.predateurs.append(i)
                        elif i.status == "SuperPredateur":
                            self.symbiose.append(i)
                if i.body.Mort and self.status == "Decomposeur":
                    print("Julie test decomposeur")
                    self.proies.append(i)
            elif isinstance(i, Item):
                i.dist = self.body.position.distance_to(i.position)
                if self.status == "Herbivore":
                    self.proies.append(i)
        self.predateurs.sort(key=lambda x: x.dist, reverse=False)
        self.proies.sort(key=lambda x: x.dist, reverse=False)
        self.symbiose.sort(key=lambda x: x.dist, reverse=False)


    def update(self):
        self.filtre_perception()
        if len(self.proies) > 0:
            if isinstance(self.proies[0], Item):
                target = self.proies[0].position - self.body.position
            else:
                target = self.proies[0].body.position - self.body.position
            self.body.acc = self.body.acc + target

        if len(self.predateurs) > 0:
            if len(self.symbiose) > 0:
                target = self.body.position + self.symbiose[0].body.position
            else:
                target = self.body.position - self.predateurs[0].body.position
            self.body.acc = self.body.acc + target

        else:
            target = Vector2(random.randint(-1, 1), random.randint(-1, 1))
            while target.length() == 0:
                target = Vector2(random.randint(-1, 1), random.randint(-1, 1))
            self.body.acc += target

    def show(self):
        # on show le body de l'agent
        self.body.show(self.status)



