import json
import random
import time
from threading import Thread

from pygame.math import Vector2
import core
from agent import Agent
from body import Body
from item import Item
import matplotlib
import matplotlib.pyplot as plt

def setup():
    print("Setup START---------")
    core.fps = 30
    core.WINDOW_SIZE = [1200, 800]
    core.memory("agents", [])
    core.memory("vegetaux", [])

    for i in range(0, 20):
        core.memory('vegetaux').append(Item(Vector2(random.randint(0, core.WINDOW_SIZE[0]), random.randint(0, core.WINDOW_SIZE[1]))))




    core.memory("startTime", time.time())
    core.memory("data", load("scenario.json"))
    core.memory("duration", core.memory("data")["dureeSimu"])

    # On ajoute les agents
    superpredateur_load()
    herbivore_load()
    carnivore_load()
    decomposeur_load()


    count_agents()
    # t1 = Thread(target=affiche_pop)
    # t1.daemon = True
    # t1.start()

    print("Setup END-----------")
    print("Pour le graphique, voir mygraph.png")


def affiche_pop():

    while True:

        plt.bar("SuperPredateur", core.memory("nb_superpredateurs"))
        plt.bar("Carnivore", core.memory("nb_carnivores"))
        plt.bar("Herbivore", core.memory("nb_herbivores"))
        plt.bar("Decomposeur", core.memory("nb_decomposeurs"))
        plt.ion()
        plt.draw()
        # on enregistre l'image dans mygraph.png
        plt.savefig("mygraph.png")
def compute_perception(agent):

    for agent in core.memory('agents'):
        agent.body.fustrum.perceptionList = []
        for b in core.memory('agents'):
            if agent.uuid != b.uuid:
                if agent.body.fustrum.inside(b.body):
                    agent.body.fustrum.perceptionList.append(b)
        for vegetal in core.memory("vegetaux"):
            if agent.body.fustrum.inside(vegetal):
                agent.body.fustrum.perceptionList.append(vegetal)
def compute_decision(agent):
    for a in core.memory('agents'):
        a.update()


def apply_decision(agent):
    for a in core.memory('agents'):
        a.body.update()


def draw():
    # Display
    for agent in core.memory("agents"):
        agent.show()

    for agent in core.memory("agents"):
        compute_perception(agent)

    for agent in core.memory("agents"):
        compute_decision(agent)

    for agent in core.memory("agents"):
        apply_decision(agent)

    for item in core.memory("vegetaux"):
        item.show()

def reset():
    core.memory("agents", [])

    for i in range(0, 5):
        core.memory('agents').append(Agent(Body()))


def load(path) :
    f = open(path)
    data = json.load(f)
    f.close()
    return data

















def superpredateur_load() :

    for i in range(0, core.memory("data")["SuperPredateur"]["nb"]):
        core.memory('agents').append(Agent(
            Body( Vector2(random.randint(0, core.WINDOW_SIZE[0]), random.randint(0, core.WINDOW_SIZE[1])),
                  random.uniform(float(core.memory("data")["SuperPredateur"]["parametres"]["vitesseMax"][0]),
                                float(core.memory("data")["SuperPredateur"]["parametres"]["vitesseMax"][1])),
                 random.uniform(float(core.memory("data")["SuperPredateur"]["parametres"]["accelerationMax"][0]),
                                float(core.memory("data")["SuperPredateur"]["parametres"]["accelerationMax"][1])),
                 random.uniform(float(core.memory("data")["SuperPredateur"]["parametres"]["MaxFaim"][0]),
                                float(core.memory("data")["SuperPredateur"]["parametres"]["MaxFaim"][1])),
                 random.uniform(float(core.memory("data")["SuperPredateur"]["parametres"]["MaxFatigue"][0]),
                                float(core.memory("data")["SuperPredateur"]["parametres"]["MaxFatigue"][1])),
                 random.uniform(float(core.memory("data")["SuperPredateur"]["parametres"]["reproduction"][0]),
                                float(core.memory("data")["SuperPredateur"]["parametres"]["reproduction"][1])),
                 )
            , "SuperPredateur")
        )

def herbivore_load():
    for i in range(0, core.memory("data")["Herbivore"]["nb"]):
        core.memory('agents').append(Agent(
            Body(Vector2(random.randint(0, core.WINDOW_SIZE[0]), random.randint(0, core.WINDOW_SIZE[1])),
                 random.uniform(float(core.memory("data")["Herbivore"]["parametres"]["vitesseMax"][0]),
                                float(core.memory("data")["Herbivore"]["parametres"]["vitesseMax"][1])),
                 random.uniform(float(core.memory("data")["Herbivore"]["parametres"]["accelerationMax"][0]),
                                float(core.memory("data")["Herbivore"]["parametres"]["accelerationMax"][1])),
                 random.uniform(float(core.memory("data")["Herbivore"]["parametres"]["MaxFaim"][0]),
                                float(core.memory("data")["Herbivore"]["parametres"]["MaxFaim"][1])),
                 random.uniform(float(core.memory("data")["Herbivore"]["parametres"]["MaxFatigue"][0]),
                                float(core.memory("data")["Herbivore"]["parametres"]["MaxFatigue"][1])),
                 random.uniform(float(core.memory("data")["Herbivore"]["parametres"]["reproduction"][0]),
                                float(core.memory("data")["Herbivore"]["parametres"]["reproduction"][1])),
                 )
            , "Herbivore")
        )

def carnivore_load():
    for i in range(0, core.memory("data")["Carnivore"]["nb"]):
        core.memory('agents').append(Agent(
            Body(Vector2(random.randint(0, core.WINDOW_SIZE[0]), random.randint(0, core.WINDOW_SIZE[1])),
                 random.uniform(float(core.memory("data")["Carnivore"]["parametres"]["vitesseMax"][0]),
                                float(core.memory("data")["Carnivore"]["parametres"]["vitesseMax"][1])),
                 random.uniform(float(core.memory("data")["Carnivore"]["parametres"]["accelerationMax"][0]),
                                float(core.memory("data")["Carnivore"]["parametres"]["accelerationMax"][1])),
                 random.uniform(float(core.memory("data")["Carnivore"]["parametres"]["MaxFaim"][0]),
                                float(core.memory("data")["Carnivore"]["parametres"]["MaxFaim"][1])),
                 random.uniform(float(core.memory("data")["Carnivore"]["parametres"]["MaxFatigue"][0]),
                                float(core.memory("data")["Carnivore"]["parametres"]["MaxFatigue"][1])),
                 random.uniform(float(core.memory("data")["Carnivore"]["parametres"]["reproduction"][0]),
                                float(core.memory("data")["Carnivore"]["parametres"]["reproduction"][1])),
                 )
            , "Carnivore")
        )

def decomposeur_load():
 for i in range(0, core.memory("data")["Decomposeur"]["nb"]):
        core.memory('agents').append(Agent(
            Body(Vector2(random.randint(0, core.WINDOW_SIZE[0]), random.randint(0, core.WINDOW_SIZE[1])),
                 random.uniform(float(core.memory("data")["Decomposeur"]["parametres"]["vitesseMax"][0]),
                                float(core.memory("data")["Decomposeur"]["parametres"]["vitesseMax"][1])),
                 random.uniform(float(core.memory("data")["Decomposeur"]["parametres"]["accelerationMax"][0]),
                                float(core.memory("data")["Decomposeur"]["parametres"]["accelerationMax"][1])),
                 random.uniform(float(core.memory("data")["Decomposeur"]["parametres"]["MaxFaim"][0]),
                                float(core.memory("data")["Decomposeur"]["parametres"]["MaxFaim"][1])),
                 random.uniform(float(core.memory("data")["Decomposeur"]["parametres"]["MaxFatigue"][0]),
                                float(core.memory("data")["Decomposeur"]["parametres"]["MaxFatigue"][1])),
                 random.uniform(float(core.memory("data")["Decomposeur"]["parametres"]["reproduction"][0]),
                                float(core.memory("data")["Decomposeur"]["parametres"]["reproduction"][1])),
                 )
            , "Decomposeur")
        )





def count_agents():
    nb_superpredateurs_mort = 0
    nb_carnivores_mort = 0
    nb_herbivores_mort = 0
    nb_decomposeurs_mort = 0
    nb_superpredateurs = 0
    nb_carnivores = 0
    nb_herbivores = 0
    nb_decomposeurs = 0

    for a in core.memory("agents"):
        if not a.body.Mort:
            if a.status ==  "SuperPredateur":
                nb_superpredateurs += 1
            elif a.status == "Carnivore":
                nb_carnivores += 1
            elif a.status == "Herbivore":
                nb_herbivores += 1
            elif a.status == "Decomposeur":
                nb_decomposeurs += 1
        nb_total = nb_superpredateurs + nb_carnivores + nb_herbivores + nb_decomposeurs
        if a.body.Mort:
            if a.status ==  "SuperPredateur":
                nb_superpredateurs_mort += 1
            elif a.status == "Carnivore":
                nb_carnivores_mort += 1
            elif a.status == "Herbivore":
                nb_herbivores_mort += 1
            elif a.status == "Decomposeur":
                nb_decomposeurs_mort += 1

    if nb_total != 0:
        core.memory("nb_superpredateurs", nb_superpredateurs/ nb_total * 100)
        core.memory("nb_carnivores", nb_carnivores/ nb_total * 100)
        core.memory("nb_herbivores", nb_herbivores/ nb_total * 100)
        core.memory("nb_decomposeurs", nb_decomposeurs/ nb_total * 100)
        core.memory("nb_superpredateurs_mort", nb_superpredateurs_mort/ nb_total * 100)
        core.memory("nb_carnivores_mort", nb_carnivores_mort/ nb_total * 100)
        core.memory("nb_herbivores_mort", nb_herbivores_mort/ nb_total * 100)
        core.memory("nb_decomposeurs_mort", nb_decomposeurs_mort/ nb_total * 100)
        core.memory("nb_total", nb_total)
        core.memory("nb_total_mort", nb_superpredateurs_mort + nb_carnivores_mort + nb_herbivores_mort + nb_decomposeurs_mort)


def meilleur_perf() :
    meilleur_vMax = 0
    meilleur_accMax = 0
    meilleur_jaugeFaimMax = 0
    meilleur_tiredMax = 0
    meilleur_reproductionMax = 0
    meilleur_esperancevie = 0
    meilleur_agent_vMax = 0
    meilleur_agent_accMax = 0
    meilleur_agent_jaugeFaimMax = 0
    meilleur_agent_tiredMax = 0
    meilleur_agent_reproductionMax = 0
    meilleur_agent_esperancevie = 0

    for a in core.memory('agents'):

        if meilleur_vMax <= a.body.vMax :
            meilleur_vMax = a.body.vMax
            meilleur_agent_vMax = a

        if meilleur_accMax <= a.body.accMax :
            meilleur_accMax = a.body.accMax
            meilleur_agent_accMax = a

        if meilleur_jaugeFaimMax <= a.body.jaugeFaimMax :
            meilleur_jaugeFaimMax = a.body.jaugeFaimMax
            meilleur_agent_jaugeFaimMax = a

        if meilleur_tiredMax <= a.body.tiredMax :
            meilleur_tiredMax = a.body.tiredMax
            meilleur_agent_tiredMax = a

        if meilleur_reproductionMax <= a.body.reproductionMax :
            meilleur_reproductionMax = a.body.reproductionMax
            meilleur_agent_reproductionMax = a

        if meilleur_esperancevie <= a.body.esperancevie :
            meilleur_esperancevie = a.body.esperancevie
            meilleur_agent_esperancevie = a

    print('ID Meilleur agent vitesse max : ',meilleur_agent_vMax.uuid)
    print('ID Meilleur agent acceleration max : ', meilleur_agent_accMax.uuid)
    print('ID Meilleur agent jauge faim max : ', meilleur_agent_jaugeFaimMax.uuid)
    print('ID Meilleur agent tired max : ', meilleur_agent_tiredMax.uuid)
    print('ID Meilleur agent reproduction max : ', meilleur_agent_reproductionMax.uuid)
    print('ID Meilleur agent esperance vie max : ', meilleur_agent_esperancevie.uuid)
def updateEnv():
    for a in core.memory("agents"):
        for p in a.proies:
            if isinstance(p, Item):
                if a.body.position.distance_to(p.position) <= a.body.mass and a.status == "Herbivore" :
                    a.body.jaugeFaim = 0
                    if p in core.memory("vegetaux") :
                        core.memory("vegetaux").remove(p)
                    a.proies.remove(p)
            else:
                if a.body.position.distance_to(p.body.position) <= a.body.mass:
                    a.body.jaugeFaim = 0
                    if p in core.memory("agents"):
                        core.memory("agents").remove(p)
                    if a.status == "Decomposeur":
                        core.memory("vegetaux").append(Item(a.body.position))
        if a.body.reproduction >= a.body.reproductionMax and not a.body.isSleeping:
            core.memory("agents").append(Agent(Body(a.body.position,
                                                    a.body.vMax,
                                                    a.body.accMax,
                                                    a.body.jaugeFaimMax,
                                                    a.body.tiredMax,
                                                    a.body.reproductionMax),
                                               a.status))
            a.body.reproduction = 0
def run():

    if core.getKeyPressList('r'):
        reset()

    core.cleanScreen()
    draw()

    count_agents()
    updateEnv()

    # affichage des pourcentage, nb
    print('Pourcentage de superprédateurs',core.memory("nb_superpredateurs"))
    print('Pourcentage de carnivores',core.memory("nb_carnivores"))
    print('Pourcentage de herbivores',core.memory("nb_herbivores"))
    print('Pourcentage de décomposeurs',core.memory("nb_decomposeurs"))
    print('Nombre total de vivants',core.memory("nb_total"))
    print('Pourcentage de superprédateurs morts', core.memory("nb_superpredateurs_mort"))
    print('Pourcentage de carnivores morts', core.memory("nb_carnivores_mort"))
    print('Pourcentage de herbivores morts', core.memory("nb_herbivores_mort"))
    print('Pourcentage de décomposeurs morts', core.memory("nb_decomposeurs_mort"))
    print('Nombre total de morts', core.memory("nb_total_mort"))
    # affichage de l'agent avec les meilleurs paramètres du body
    meilleur_perf()




core.main(setup, run)